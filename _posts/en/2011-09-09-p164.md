---
layout: post
lang: en
category: en
title: 'Software Freedom Day soon in Prishtina'
author: "altin"
---
![alt](/flossk.org/img/blog/sfd.png)
Software Freedom Day (SFD) is an annual worldwide celebration of Free Software. SFD is a public education effort to raise awareness of Free Software and its virtues, and also encourage its use.

And this time is the second year we are organizing something like this.

Basically there will be nothing special, only some presentations individually and having fun.

The event will happen in Prishtina, Kosovo, likewise everywhere else that this event is organized on 17th, this time it will happen on 18th of September, 2011. The meeting will be held in Prince Coffee House at 8PM (Prishtina local time)You can also accept you're attendance on Facebook

Prince Coffee House on map:

View Larger Map
