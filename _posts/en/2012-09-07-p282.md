---
layout: post
lang: en
category: en
title: 'Workshop-Virtual Server @InnovationsLab (Dashamir Hoxha)'
author: "BleonaFoniqi"
---
Dashamir Hoxha is a Software engineering, anayst/designer, programer, web application developper, linux server administration, network engineering, configuration manager (CVS/SVN admin), documentation writer etc.

Topics:

- Virtual Machines on a CentOS Host : http://dashohoxha.posterous.com/virtual-machines-on-a-centos-host
- Improving the Performance of a VM by Using a Real Disk Partition :http://dashohoxha.posterous.com/improving-the-performance-of-a-vm-by-using-a
- Virtual Machine Management on Ubuntu : http://dashohoxha.posterous.com/virtual-machine-management-on-ubuntu
