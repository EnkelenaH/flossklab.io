---
layout: post
lang: en
category: en
title: 'FLOSSK at ITTF'
author: "admin"
---
![alt](/flossk.org/img/blog/img7.jpg)
FLOSSK used this year's edition of the Information Technology Fair (ITTF)  held in Prishtina to present our organization and promote Free and Open  Source Software to visitors at the fair.

FLOSSK’s table was surrounded with interested people. In addition there were also companies and other local and foreign organizations that had the opportunity to talk with the members of FLOSS Kosova commending the work of our young volunteers, some also leaving their contact information and asking to be called for help when needed.

At this fair we explained to the visitors our work as an organization and the idea of Free and Open Source Software movement.

Many visitors were also interested to have free software courses in Kosovo. We mentioned our next annual conference and the arrival of Richard Stallman in Kosovo.

Click here to view the photo gallery of this event.

For more watch the video (sq only) interview made by the Radio Television of Kosovo.
