---
layout: post
lang: en
category: en
title: 'Mozilla Project in FLOSSK'
author: "admin"
---
![alt](/flossk.org/img/blog/mozilla.jpg)

Mozilla is also one of the main projects that FLOSSK members are giving their contribution on promoting Mozilla and its community, spreading its products and translating them, so that people can feel more comfortable on the Internet.

Our organization even has three young people who are representatives of Mozilla Project here in Kosovo, and they are continually very active on the Mozilla Balkans Community team.

- Altin Ukshini
- Gent Thaci
- Heroid Shehu

The Mozilla Foundation is a non-profit organization that promotes openness, innovation and participation on the Internet.

Mozilla is best known for the browser Firefox, but they advance their mission through other software projects, grants and engagement efforts such as Mozilla Drumbeat.

Although thousands of people are part of Mozilla, the Mozilla Foundation itself is a small team of people. Mozilla provides core services to the Mozilla community and promotes the values of an open Internet to the broader world.

If you're interested in supporting Mozillas efforts, please consider making a donation and getting involved with the Mozilla community.

Mozilla Projects:

The Mozilla community produces a lot of great software and acts as an incubator for innovative ideas as a way to advance their mission of building a better Internet.

These applications are developed by the Mozilla community and their code is hosted on mozilla.org.

The most used Mozilla Applications are: Firefox for Desktop, Firefox for Mobile, Thunderbird, SeaMonkey, Lightning and Sunbird

Then probably the most used applications from web developers: Bugzilla, Firebug, and much more of them.

For more about Mozilla visit their home page:http://www.mozilla.org/
Or even listen to this video:http://www.mozilla.org/about/
