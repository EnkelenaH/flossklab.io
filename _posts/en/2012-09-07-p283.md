---
layout: post
lang: en
category: en
title: 'Workshop-Zend Framework @InnovationsLab (Flamur Mavraj)'
author: "BleonaFoniqi"
---
Flamur is the founder and lead developer at Empirio AS, a small company with focus on web applications using modern techniques and methods for rapid development. He has worked for some large Norwegian companies in the past, such as Nettby Community AS, Akershus County Council, Holtebyggsafe AS and HP Norway. He is also taking a degree on computer science and mathematics at University of Oslo.

Parts of the framework:

- Setting up a skeleton application (Zend\Mvc)
- Creating a module (Zend\Mvc)
- Creating a controller and action
- Setting up Zend\Db
- Using Zend\Form with different approach
- Using validations and view helpers to render the form
