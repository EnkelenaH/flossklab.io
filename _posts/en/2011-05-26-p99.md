---
layout: post
lang: en
category: en
title: 'OLPC Project'
author: "admin"
---
![alt](/flossk.org/img/blog/olpc-square_logo.jpg)
OLPC project has been present since the beginnings of this organization, when our members continued to promote this educational project starting from various presentations in educational institutions and along to the annual Conference “Software Freedom Kosova”organized by FLOSSK.
	
One Laptop Per Child “OLPC” is intended to empower the poorest children of the world through providing education to every child with a laptop that has low costs and low power consumption.

For this purpose, the OLPC project designed hardware and software applications usable by the children to learn together. With access to this kind of tool, children engage in self-education: learn, collaborate and create together. By connecting with each other they relate to the world for a brighter future.

OLPC laptop has an operating system based on Fedora Linux, Sugar interface and has various applications suitable for low learning age.

Laptops have wireless internet connection that allow standard access points or networks 'mesh', which enables the connection of each laptop with teammates and friends for cooperation. This kind of interface can be downloaded from the internet and you can try it on your computer.

If you want to know more about this project visit: http://one.laptop.org/

Principles and Child Empowerment:

The XO Laptop, desighn for learning:
