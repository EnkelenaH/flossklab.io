---
layout: post
lang: en
category: en
title: 'Fedora Project in FLOSSK'
author: "admin"
---
![alt](/flossk.org/img/blog/fedora-logo.png)
The Fedora Project is a partnership of community members of Free and Open Source Software from around the world. The Fedora Project builds communities of Free and Open Source Software and produces an operating system based on GNU / Linux, called Fedora.

FLOSSK at any launch of new versions of Fedora with the help of  FedoraProject community organizes events and gives CDs of this operating system

FedoraProject has also two Official Ambassadors in Kosovo -Ardian Haxha and Gent Thaci - who are also members of FLOSSK

More about FedoraProject: https://fedoraproject.org/https://fedoraproject.org/wiki/http://en.wikipedia.org/wiki/Fedora_%28operating_system%29https://fedoraproject.org/wiki/Releases/13/FeatureList
