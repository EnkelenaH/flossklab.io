from lxml import html
import requests
import sys
import os
import re
from dateutil.parser import parse

page = requests.get("http://flossk.org/node/"+sys.argv[1])
#page = requests.get("http://flossk.org/sq/blog/p%C3%ABr-programin-qeveris%C3%ABs-kosov%C3%ABs-roli-flossk-ut-zhvillimin-e-ekonomis%C3%AB-dhe-pavar%C3%ABsis%C3%AB")
tree = html.fromstring(page.content)
try:
    title = tree.xpath('//*[@id="page-title"]')[0].text_content()
    author = tree.xpath('//*[@class="username"]')[0].text_content()
    date =  tree.xpath('//*[@property="dc:date dc:created"]')[0].attrib["content"]
    images =  tree.xpath('//*[@class="field field-name-body field-type-text-with-summary field-label-hidden"]//img')
    content = tree.xpath('//*[@class="field field-name-body field-type-text-with-summary field-label-hidden"]')[0].text_content()
except:
    sys.exit()
m = date[:date.index("T")]

file = open(m+"-p"+sys.argv[1]+".md","w") 
file.write("---\n")
file.write("layout: post\n")
file.write("title: \'" + title.strip().replace("''","\'").replace('"','\"')+"\'\n")
file.write("author: \"" + author.strip().replace('"','\"')+"\"\n")
file.write("---\n")
for image in images:
#    print(image.attrib["src"])
    file.write("![alt](/flossk.org/img/blog/"+os.path.basename(image.attrib["src"].replace("?",""))+")\n")
file.write(content.strip()+"\n")
file.close()
