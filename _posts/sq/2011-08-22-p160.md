---
layout: post
title: 'Ligjerata e Dan McQuillan në Prishtinë'
lang: sq
category: sq
author: "altin"
---
![alt](/flossk.org/img/blog/dan_thumb.jpg)
Dan McQuillan, aktivist i njohur i rrjeteve sociale dhe fitues i çmimit “Global Ideas Bank Social Innovations Award”, në një ligjëratë të mbajtur sot në ambientet e UNICEF Innovations Lab Kosovo, dha disa detaje interesante në lidhje me ndikimin e rrjeteve sociale në jetën e përditshme dhe në lëvizjet e përgjithshme shoqërore botërore.

		
		Para një audience prej dhjetëra entuziastëve të rinj dhe me ftesën e FLOSSK, McQuillan bëri një përshkrim të shkurtër të angazhimeve të tija dhe përditshmërisë së tij, dhe shpjegoi sesi përmes “Social Innovatio Camp” bashkohen ide, njerëz e vegla digjitale për të ndërtuar zgjidhje me bazë në ueb për probleme shoqërore, dhe atë brenda 48 orëve.

		Edhe pse me një prezantim paksa më thumbues ndaj botës së stratup, McQuillan vazhdoi më tutje duke treguar për shembuj të suksesshëm të ideve të krijuara për të ndihmuar komunitetin, siç ishte rasti me një ueb faqe dizajni për persona me aftësi të kufizuara në Angli, të cilët mund të shfrytëzojnë këtë faqe për të sugjeruar dizajne më të mira për disa mjete të shumta të tyre që shpesh ka një dizajn të dobët. 


	Më pas, ligjërata e quajtur “Lidhja mes inovacionit, hacking dhe lëvizjeve të reja sociale” vazhdoi tutje me shpjegimin e termit hacking dhe rëndësinë e tij në nivel shoqëror, e jo në kuptim negativ. “Me hacking nuk nënkuptohet vetëm diçka që duhet të thyhet, apo vetëm një term hakersh... por hacking mund të përdorim edhe për të shpjeguar mënyra të reja të shfrytëzimit të një vegle të caktuar ... ose për shembull hacking mund të quhet edhe procesi i demontimit të një gjësendi që veçse kemi në posedim, dhe kompletimi i sërishëm i tij për të funksionuar më mirë” tha ai. 

	McQuillan shprehi edhe opinionet e tija në lidhje me haktivizmin dhe lëvizjet e reja të hakerëve si Anonymous, të cilat lëvizje në mes tjerash po shërbejnë edhe për të vepruar në ndihmë të komunitetit, siç është rasti me ndërtimin e veglave virtuale për të ndihmuar qasjen në internet tek njerëzit që po bllokohen nga qeveritë e tyre, si rasti në Egjipt apo Libi. 

	Në fund të ligjëratës të pranishmit parashtruan disa opinione të tyre dhe bënë një diskutim të lirë, me mysafirin tonë.

	/pcworldalbanian
