---
layout: post
title: 'Parti i Lansimit të Fedora 13'
lang: sq
category: sq
author: "admin"
---
E Enjëte, 27 Maj, 2010 ishte një ndër eventet tjera të FLOSS Kosovës, me rastin e lëshimit të versionit të Fedora 13 FLOSSK e organizoi këtë ngjarje në lokalet e IDI (Information Development Initiative), ku pati prezentim te gjatë reth Fedora 13, historisë së themelimit të fedoras dhe shumë gjera tjera.

Edhe në këtë event si në shumicën e eventeve tjera anëtarët e FLOSSK-ut shpërndanë shumë CD  te distribucionit te Fedoras me versionin tashmë të ri Fedora 13, e gjitha kjo sigurisht për të promovuar mëtutje Softuerin e Lirë dhe me kod burimor të hapur.

Të pranishmit ishin më se të kënaqur me eventin dhe thanë së këso lloj eventesh duhen të mbahen më shpesh për arsye që FLOSSK dhe kontribuesit tjerë të Softuerit te lirë, të njihen nga komuniteti Kosovar...

Më shumë për Fedora-n lexoni në:https://fedoraproject.org/https://fedoraproject.org/wiki/http://en.wikipedia.org/wiki/Fedora_%28operating_system%29https://fedoraproject.org/wiki/Releases/13/FeatureList