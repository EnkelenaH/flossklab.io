---
layout: post
title: 'FOSS BootCamp në Mitrovicë - 26 Prill'
lang: sq
category: sq
author: "altin"
---
FLOSSK me ndihmën e UNICEF Innovations Lab Kosovo do të organizojë FOSS BootCamp tani në tre qytete të ndryshme të Kosovës: Pejë, Gjakovë dhe Mitrovicë.

FOSS BootCamp është një ngjarje që përmban një seri të punëtorive brenda të cilave zhvillohen aktivitete të ndërlidhura me edukimin në fushën e softuerit me kod burimor të hapur (Open Source Software).

Të dizajnuara për fillestarë, këto punëtori kanë për qëllim të paraqesin një hyrje të përgjithshme për disa vegla dhe platforma me kod burimor të hapur.

Ndërkohë që FOSS BootCamp i dytë do të mbahet këtë të shtunë në Gjakovë, ne po hapim aplikimet edhe për pjesëmarrësit në BootCamp-in e Mitrovicës i cili do të mbahet më datën 26 Prill, nga ora 10:00AM në International Business College Mitrovica (IBCM).

Të gjithë të interesuarit nga Mitrovica mund të jenë pjesë e kësaj ngjarjeje. Pjesëmarrësit do të marrin certifikatë pjesëmarrjeje për punëtoritë e ndjekura.

Aplikimi bëhet online, ju lutemi ndiqni vegzën në vijim: http://goo.gl/HoFG83

Falënderojmë IBCM për mundësimin e mbajtjes së këtyre punëtorive në ambientet e tyre.

Ndajeni lajmin, dhe aplikoni sa më shpejt!

Për çdo pyetje ju lutem drejtohuni në [email protected]