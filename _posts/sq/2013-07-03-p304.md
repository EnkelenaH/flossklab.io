---
layout: post
title: 'WMKIT workshop në Universitetin e Prizrenit'
lang: sq
category: sq
author: "besfortguri"
---
![alt](/flossk.org/img/blog/WMKIT_presentation_Open_Labs_Prizren_small.jpg)
![alt](/flossk.org/img/blog/Open_Labs_prizren_small.jpg)
Pas një sër prezentimesh në Tiranë, Athinë, Prishtinë dhe Kretë të Greqisë, WMKIT vjen edhe në qytetin historik të Prizrenit.

Qëllimi i kësaj ngjarje ishte mësmi rreth elektronikës dhe programimt. Open Labs dhe FLOSSK së bashku ishin organizatorë të kësaj ngjarje.

Të pranishëm ishin rreth 100 studentë nga Univerziteti i Prizrenit të ndarë në 2 klasë që ndoqën prezantimet dhe demonstrimet 40 minutëshe të bra nga anëtarët e projektit të WMKIT.
 
Nëse shprehni dëshirë të organizohet një workshop i tillë në qytetin apo komunitetin tënd ju lutem na kontaktoni përmes emailëve tanë dhe ne do të diskutojmë mundësitë për të ardhur në qytetin tuaj.