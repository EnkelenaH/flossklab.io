---
layout: post
title: 'Parti i Lansimit të KDE SC 4.4'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/kde-rp.jpg)
Me rastin e lansimit të ri të KDE SC 4.4 Flossk organizoi një festë më datën 09/02/2010 ne lokalet e IDI ku te pranishem ishin më shumë se 20 adhurues të Softuerit të lirë dhe të hapur e veqanerisht përdorues te KDE-së.

Hapjen e këti organizimi e bëri Milot Shala me Prezantimin e tij për KDE SC 4.4

Gjithashtu të ftuarit u mirpritën me nje gosti.

Për më shumë shiqojeni galerinë: Kliko Ketu
