---
layout: post
title: 'SFD në Gjakovë'
lang: sq
category: sq
author: "admin"
---
FLOSSK Gjakova, ekipi lokal i organizatës FLOSSK, më 18 Shtator organizoi ngjarjen Software Freedom Day (SFD) 2010 në qytetin e Gjakovës.
Qëllimi i kësaj ngjarje ishte që edhe në këtë qytet të fillojë të përhapet softueri i lirë e GNU Linux. Në të njëjtën kohë, me këtë aktivitetet synohej që të rritej komuniteti lokal i cili do angazhohet për softuerin e lirë dhe me burim të hapur.
Ideja për shënimin dhe organizimin Software Freedom Day, u bë në baza vullnetare të organizatës.
Te Interesuarit që ishin të pranishëm patën rastin te njoftohen reth Softuerit të Lirë, GNU/Linux, Njohurisë së Hapur, Burimit të Hapur dhe shumë gjëra tjera që ndërlidhen më të.
Gjatë kësaj ngjarje u dhanë ligjërata mbi:

		GNU/Linux,

		Instalimin e konfigurimin e distribucionit Ubuntu dhe si gjithnjë

		Shpërndarja e CD-ve me këtë distribucion.
Në mesin e shumë çështjeve të softuerit të lirë, ngjarja po ashtu sqaroi edhe dallimet në mes të GNU/Linux me atë Windows dhe poashtu prezantoi softuerë të lirë me të cilët mund të bëhet dizajn grafik si GMIP, Inkscape, per 3D Blender e programe tjera...
Më shumë rreth software freedom day vizitoni:http://softwarefreedomday.org/
