---
layout: post
title: 'Hartografimi i Objekteve Publike të Kosovës'
lang: sq
category: sq
author: "altin"
---
![alt](/flossk.org/img/blog/mpik-thumb.png)
Një projekt tjetër që kemi qenë duke punuar është Hartografimi i institucioneve/objekteve publike në Kosovë, projekt i cili është mbështetur nga UNICEF Innovations Lab. Qëllimi kryesor i këtij projekti ishte siq u cek edhe më lartë hartografimi i të gjitha objektet ekzistuese publike në Kosovë. Përderisa ne e kishim nisur rrugën e gjatë për të arritur qëllimin tonë, mungesa e informacionit ka qenë një pengesë e madhe.
Ne do të donim të falënderojmë një projekt tjetër të lirë - InformataZyrtare.org - e cila na pajisi me informata për të gjitha institucionet, në kontrast me ministrinë e cila nuk e bëri një gjë të tillë. Ne hartografuam në shtatë qytetet më të mëdha të Kosovës, të gjitha të dhënat që kemi mbledhur janë lirisht të qasshme në www.osm.org OpenStreetMap.
Si e bëmë atë? Ne kemi vënë së bashku tetë ekipe me të rinj të punojnë në mbledhjen e të dhënave dhe redaktimin e hartës. Çdo ekip kishte dy anëtarë. Ata kishin një ditë trajnimi për këtë proces dhe çfarë pritej prej tyre.
Njerëz të rëndësishëm në projekt ishin Lulzim Gashi si menaxhues i projektit, Valon Brestovci, Besfort Guri dhe Gent Thaçi.
Të dhënat e mbledhura do të publikohet së shpejti si një file CSV apo diçka të ngjashme!
