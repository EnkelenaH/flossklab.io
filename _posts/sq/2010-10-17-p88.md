---
layout: post
title: 'Mozilla Day në Gjakovë'
lang: sq
category: sq
author: "admin"
---
Për herë të parë në Kosovë, më 16 Tetor 2010 FLOSSK Organizon Mozilla Day në qytetin e Gjakovës, ne Biblotekën “Ibrahim Rugova” Gjakovë.

Në ktë event u fol kryesisht për Mozilla’n dhe Programet për web nga Mozilla si:

- Firefox e Thunderbird, poashtu u fol edhe për projektet e përhapjes së Firefox sikur SpreadFirefox,
- Mozilla Camp Reps,
- progresi me Firefox 4.0
- Lokalizimi, përkthimi në shqip i Firefox
- Thunderbird por pa u lënë anash as Add-Ons-at e Firefox-it.

Prezantues në këtë ngjarje ishin

- Heroid Shehu,
- Jon Berisha dhe
- Endrit Vula.

Për më shumë rreth Mozilla’s vizitoni: http://www.mozilla.org