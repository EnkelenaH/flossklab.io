---
layout: post
title: 'Ejani në Partin e Lansimit të Fedora 15 - Së shpejti'
lang: sq
category: sq
author: "admin"
---
![alt](/flossk.org/img/blog/fedora-logo.png)
Fedora 15, koduar "Lovelock", u lansua më 24 Maj.

	Fedora është  sistem operativ i Lirë dhe me kod të hapur që vazhdon të ofroj risi për përdoruesit në mbarë botën, dhe me freskim çdo gjashtë muaj. Fedora kësaj radhe erdhi së bashku me shumë gjëra të reja duke përfshirë edhe  systemd  si zëvendësim për SysVinit dhe Upstart që vepron si menaxhgjer sistemi dhe sesioni, GNOME 3.0, suitën LibreOffice dhe shumë më tepër.

	Ashtu si në shumë vende rreth botës, edhe FLOSSK do të organizojë një ndejë/parti me rastin e lansimint të këtij versioni të ri të Fedora-s që do të ndodhë në Prishtinë më 8 Korrik.

	Ju lutem ndiqni linkun për më shumë informacion http://fedoraproject.org/wiki/Release_Party_F15_Prishtina
