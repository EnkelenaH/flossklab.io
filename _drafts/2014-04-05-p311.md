---
layout: post
title: 'FOSS BootCamp në Pejë - 13 Prill'
author: "altin"
---
FLOSSK me ndihmën e UNICEF Innovations Lab Kosovo do të organizojë FOSS BootCamp tani në tre qytete të ndryshme të Kosovës; Pejë, Gjakovë dhe Mitrovicë.
FOSS BootCamp është një ngjarje që përmbanë një seri të punëtorive brenda të cilave zhvillohen aktivitete ndërlidhur me edukimin në fushën e softuerëve me kod burimor të hapur (Open Source Software).
E dizajnuar për fillestarë, këto punëtori kanë për qëllim të paraqesin një hyrje të përgjithshme për disa vegla dhe platformat me kod burimor të hapur.
FOSS BootCamp i parë do të mbahet në Pejë më datën 13 Prill, nga ora 10:00AM në Qendrën Rinore "Atë Lorenc Mazrreku".
Të gjithë të studentët e interesuarit nga Peja mund të jenë pjesë e kësaj ngjarjeje.
Pjesmarësit do marin certifikatë pjesmarjeje për punëtoritë e ndjekura.
Aplikimi bëhet online, ju lutem ndiqni linkun në vijim:http://goo.gl/HoFG83
Falënderojmë Qendrën Rinore "Atë Lorenc Mazrreku" për mundësimin e mbajtjes së këtyre punëtorive në ambientet e qendrës.
Vëmendje: Datat për BootCamp-et në Gjakovë dhe Mitrovicë do të publikohen shumë shpejt!
Ndajeni lajmin, dhe aplikoni sa më shpejt!
Për ç'do pyetje ju lutem drejtohuni në [email protected]
