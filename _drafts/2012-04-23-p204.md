---
layout: post
title: 'Tech Talk with Mr.Fabio Biscaro'
author: "altin"
---
![alt](/flossk.org/img/blog/tech_talk2_en.jpg)
Innovations Lab and FLOSSK invites you to the second Tech Talk to be held on Tuesday, April 24 2012 at 6pm in the Innovations Lab premises.
Our guest speaker is Mr. Fabio Biscaro, from Italy, who is going to talk about the importance and challenges of team work in software development and the suitability of SCRUM as an agile methodology to get the best out of software development in teams.
	Mr. Biscaro, has worked for WebScience in italy, heading the office of Treviso and managed the team that contributed to the open source Business Intelligence application - - Pentaho. Mr. Biscaro was teaching assistant at Politecnico di Milano.
	He also has experience in software quality assurance and will give examples of how this is done with SCRUM and in open source applications.
Resources:
	The website of the WebScience http://www.webscience.it/
	Pentaho - http://www.pentaho.com/get-started/?gclid=CN6ZjpyTy68CFYnO3wod13-iZQ
In the map below you can find the exact location of Unicef Innovations Lab office:


View Larger Map
