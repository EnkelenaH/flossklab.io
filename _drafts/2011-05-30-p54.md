---
layout: post
title: 'FLOSSK at NSND Ohrid'
author: "altin"
---
![alt](/flossk.org/img/blog/nsnd_ohrid_thumb.jpg)
On Saturday, May 28th, FLOSSK was Invited to an un-conference meeting held in Ohrid, Macedonia called NSND - Nista Se Nece Dogoditi (Nothing Will Happen).
	Just like the name says it, nothing big happened.

	
	First we met the participants of the gathering, and then went to a restaurant to further get to know each other and eat dinner. The next day we gave some presentations, where everyone showed to the participants some tips tricks and also shared some of their knowledge learned on the job. There was some code done and also small hacks. Flosskies did a presentation too. There were more than 30 people present and we would especially like to mention the good time we had together.

	NSND - Nothing will happen is an annually held un-conference first organized in Croatia. The main of this un-conference is organizing a hackers, and especially Free Software contributors, meetup from all over ex-Yougoslavia.

	For more about NSND please visit the official page.

	To view the gallery of this event, click here.
